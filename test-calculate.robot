*** Settings ***
Library           RequestsLibrary


*** Keywords ***

Get Calculation JSON
    [Arguments]    ${num1}    ${num2}
    ${resp}=     GET    http://localhost:8000/plus/${num1}/${num2}

    Should Be Equal    ${resp.status_code}    ${200}

    RETURN    ${resp.json()}


*** Test Cases ***

Test Calculate Numbers 0 and 0 (ฺBefore Using Keywords)

    ${resp}=     GET    http://localhost:8000/plus/0/0

    Should Be Equal    ${resp.status_code}    ${200}

    ${json_resp}=    Set Variable  ${resp.json()}

    Should Be Equal    ${json_resp['result']}    ${0}

Test Calculate Numbers 4 and 2 (ฺBefore Using Keywords)

    ${resp}=     GET    http://localhost:8000/plus/4/2

    Should Be Equal    ${resp.status_code}    ${200}

    ${json_resp}=    Set Variable  ${resp.json()}

    Should Be Equal    ${json_resp['result']}    ${6}

Test Calculate Numbers -12 and 5 (ฺBefore Using Keywords)

    ${resp}=     GET    http://localhost:8000/plus/-12/5

    Should Be Equal    ${resp.status_code}    ${200}

    ${json_resp}=    Set Variable  ${resp.json()}

    Should Be Equal    ${json_resp['result']}    ${-7}

Test Calculate Numbers -12 and -700 (ฺBefore Using Keywords)

    ${resp}=     GET    http://localhost:8000/plus/-12/-700

    Should Be Equal    ${resp.status_code}    ${200}

    ${json_resp}=    Set Variable  ${resp.json()}

    Should Be Equal    ${json_resp['result']}    ${-712}

Test Calculate Numbers 'ass' and 5 (ฺBefore Using Keywords)

    ${resp}=     GET    http://localhost:8000/plus/ass/5

    Should Be Equal    ${resp.status_code}    ${200}

    ${json_resp}=    Set Variable  ${resp.json()}

    Should Be Equal As Strings    ${json_resp['result']}    Can not plus those number.

Test Calculate Numbers 'text' and '54text' (ฺBefore Using Keywords)

    ${resp}=     GET    http://localhost:8000/plus/text/54text

    Should Be Equal    ${resp.status_code}    ${200}

    ${json_resp}=    Set Variable  ${resp.json()}

    Should Be Equal As Strings    ${json_resp['result']}    Can not plus those number.